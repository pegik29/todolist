export default function () {
  return {
    categories: [
      {
        name: 'personal',
        color: 'blue',
      },
      {
        name: 'business',
        color: 'orange',
      },
    ],
  }
}
