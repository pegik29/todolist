import {AddressbarColor} from 'quasar'

export default () => {
  AddressbarColor.set('#c51162')
}
