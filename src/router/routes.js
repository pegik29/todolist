const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {path: '', component: () => import('pages/Todo.vue'), name: 'all'},
      {path: '/about', component: () => import('pages/About.vue')},
      {path: '/personal', component: () => import('pages/Todo.vue'), name: 'personal'},
      {path: '/business', component: () => import('pages/Todo.vue'), name: 'business'},
      {path: '/favorites', component: () => import('pages/Todo.vue'), name: 'favorites'},
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
